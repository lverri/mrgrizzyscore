<?php
/**
 * Created by PhpStorm.
 * User: Lorenzo
 * Date: 29/11/14
 * Time: 17:29
 */

/**
 * Paths
 */
$path = Array();
$path['root'] = dirname(dirname(__FILE__));
$path['sys'] = dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'sys' . DIRECTORY_SEPARATOR;
$path['sysModel'] = dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'sys' . DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR;
$path['sysHelper'] = dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'sys' . DIRECTORY_SEPARATOR . 'helper' . DIRECTORY_SEPARATOR;


/**
 * Debugging data
 */
$DEBUG = false;
$DEBUG = $DEBUG ? true : false;
ini_set('display_errors', $DEBUG);

/**
 * Database Connection Data
 */
$dbData['username'] = "root";
$dbData['password'] = "";
$dbData['hostname'] = "localhost";
$dbData['dbName'] = "MrGrizzy";
