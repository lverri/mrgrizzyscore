<?php
/**
 * Created by PhpStorm.
 * User: Lorenzo
 * Date: 29/11/14
 * Time: 17:24
 */
require('config' . DIRECTORY_SEPARATOR . 'config.php');
require $path['sys'] . 'ScoringSystem.php';

if(isset($_GET)){

//    print crypt($_GET['password']);
//    TODO change to $_POST later.
    $scSys = new ScoringSystem($_POST, $dbData);
    echo $scSys->Authenticate();
    $scSys->Execute();

    $resultEncapsulated = $scSys->Encapsulate();
    if ($resultEncapsulated != null) {
        print_r($resultEncapsulated);
    }
} else {
    echo "Request is not valid";
}