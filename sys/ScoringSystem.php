<?php
/**
 * Created by PhpStorm.
 * User: Lorenzo
 * Date: 29/11/14
 * Time: 17:29
 */


require $path['sysHelper'] . 'MysqliDb.php';
require $path['sysModel'] . 'Level.php';
require $path['sysModel'] . 'Player.php';
require $path['sysModel'] . 'Player_plays_level.php';


class ScoringSystem
{

    private $dbHelper;
    private $request;
    private $result;
    private $user;

    /**
     * Connect
     */
    public function __construct($request, $dbData)
    {
        $r = Array();
        $r['email'] = $request['email'];
        $r['password'] = $request['password'];
        $r['type'] = $request['type'];
        $r['data'] = $request['data'];

        $this->request = $r;
        $this->dbHelper = new MysqliDb($dbData['hostname'], $dbData['username'], $dbData['password'], $dbData['dbName']);
        $this->request = $request;
    }

    /**
     * Authenticate the user
     */
    public function Authenticate()
    {
        $result = false;

        $this->dbHelper->where('email', $this->request['email']);
        $user = $this->dbHelper->get('Player')[0];


        if (!$user) return "User not found.";
        if (crypt($this->request['password'], $user['password']) != $user['password']) return "Password is wrong.";

        $this->user = $user;

        if ($result) {
            return true;
        }
        return false;

    }

    /**
     * Executes the request
     */
    public function Execute()
    {
        $this->result = null;

        if ($this->request['type'] == "test") {
            echo "True";
            return true;
        }

        $data = $this->request['data'];
        $data = json_decode($data);


        switch ($this->request['type']) {

            case "get":
                /**
                 * Data should have the following format:
                 * {
                 * "level" : int
                 * "userAgent" : string
                 * "difficulty" : string -> easy, normal, hard, tough
                 * "score" : int
                 * (Optional) "nationality" : true
                 * }
                 */
                $this->dbHelper->where('Level_idLevel', $data->level);
                $this->dbHelper->where('user_agent', $data->userAgent);
                $this->dbHelper->where('difficulty', $data->difficulty);

                if (isset($data->nationality)) {
                    $this->dbHelper->join('player p', 'pg.Player_idPlayer=p.idPlayer', 'right');
                    $this->dbHelper->where('p.nationality', $this->user['nationality']);
                }

                $numberOfPlayedLevel = $this->dbHelper->getOne('player_plays_level pg', 'count(*)');
                $this->result['playedInTotal'] = $numberOfPlayedLevel['count(*)'];

                if (isset($data->nationality)) {
                    $this->dbHelper->join('player p', 'pg.Player_idPlayer=p.idPlayer', 'right');
                    $this->dbHelper->where('p.nationality', $this->user['nationality']);
                }

                $this->dbHelper->where('Level_idLevel', $data->level);
                $this->dbHelper->where('user_agent', $data->userAgent);
                $this->dbHelper->where('difficulty', $data->difficulty);
                $this->dbHelper->where("Player_idPlayer != {$this->user['idPlayer']}");
                $this->dbHelper->where('score', $data->score, '<');
                $numberOfPlayedLevel = $this->dbHelper->getOne('player_plays_level pg', 'count(*)');
                $this->result['numberPlayersWhoPlayedWorseGame'] = $numberOfPlayedLevel['count(*)'];
                /**
                 * Calculates the player position in the chart.
                 * Total Number - Number of games playedByOthers;
                 * EG: On top of 100 players, 62 played a worse game than yours. You therefore
                 * are 38th.
                 */
                $this->result['rank'] = $this->result['playedInTotal'] - $this->result['numberPlayersWhoPlayedWorseGame'];
                /**
                 * If extras want to be shown than return a list of 10 players.
                 * @error Returns double results if not use left join. Strange...
                 *
                 *
                 * 4 Lines after
                 */
                if (isset($data->nationality)) {
                    $this->dbHelper->where('p.nationality', $this->user['nationality']);
                }
                $this->dbHelper->join('player p', 'pg.Player_idPlayer=p.idPlayer', 'right');
                $this->dbHelper->where('user_agent', $data->userAgent);
                $this->dbHelper->where('score', $data->score, '<=');
                $this->dbHelper->where('Level_idLevel', $data->level);
                $this->dbHelper->orderBy('score', 'DESC');
                $this->dbHelper->where("Player_idPlayer != {$this->user['idPlayer']}");
                $playersBelow = $this->dbHelper->get('player_plays_level pg', 5, 'p.username, pg.score');
                /**
                 *  and 4 lines before
                 */
                if (isset($data->nationality)) {
                    $this->dbHelper->where('p.nationality', $this->user['nationality']);
                }

                $this->dbHelper->where('user_agent', $data->userAgent);
                $this->dbHelper->join('player p', 'pg.Player_idPlayer=p.idPlayer', 'right');
                $this->dbHelper->where('score', $data->score, '>');
                $this->dbHelper->where('Level_idLevel', $data->level);
                $this->dbHelper->orderBy('score', 'DESC');
                $this->dbHelper->where("Player_idPlayer != {$this->user['idPlayer']}");

                $playersAbove = $this->dbHelper->get('player_plays_level pg', 4, 'p.username, pg.score');
                /**
                 * Put them in the result object
                 */
                $this->result['players_above'] = $playersAbove;
                $this->result['players_below'] = $playersBelow;
                break;
            case "update":
                /**
                 *   Check if the game exists already. If so, update the score!
                 * Data should have the following format:
                 * {
                 * "level" : int
                 * "userAgent" : string
                 * "difficulty" : string -> easy, normal, hard, tough
                 * "score" : int
                 * (Optional) "nationality" : true
                 * }
                 */
                $this->dbHelper->where('Level_idLevel', $data->level);
                $this->dbHelper->where('Player_idPlayer', $this->user['idPlayer']);
                $this->dbHelper->where('user_agent', $data->userAgent);
                $this->dbHelper->where('difficulty', $data->difficulty);
                $this->dbHelper->where('score', $data->score, '<=');

                $found = $this->dbHelper->getOne('player_plays_level');

                if ($found) {
                    $this->dbHelper->where('Level_idLevel', $data->level);
                    $this->dbHelper->where('Player_idPlayer', $this->user['idPlayer']);
                    $this->dbHelper->where('user_agent', $data->userAgent);
                    $this->dbHelper->where('difficulty', $data->difficulty);

                    $toUpdate = Array(
                        'score' => $data->score
                    );
                    $update = $this->dbHelper->update('player_plays_level pg', $toUpdate);
                    $this->result = $update;
                    echo "Updated";
                } else {
                    /**
                     * else insert it.
                     */

                    $toInsert = Array(
                        'Player_idPlayer' => $this->user['idPlayer'],
                        'Level_idLevel' => $data->level,
                        'user_agent' => $data->userAgent,
                        'difficulty' => $data->difficulty,
                        'score' => $data->score,
                        'time' => $this->dbHelper->now()
                    );
                    $this->result = $this->dbHelper->insert('player_plays_level', $toInsert);
                    echo "Inserted";
                }

                break;
        }

    }

    /**
     * Returns the result in json format
     */
    public function Encapsulate()
    {
        return json_encode($this->result);
    }

    /**
     *  Register a new user
     */
    public function Register()
    {

        $this->request['username'] = mysql_real_escape_string($this->request['username']);
        $this->request['email'] = mysql_real_escape_string($this->request['email']);
        $this->request['nationality'] = mysql_real_escape_string($this->request['nationality']);

        $data = Array(
            'username' => $this->request['username'],
            'email' => $this->request['email'],
            'password' => crypt($this->request['password']),
            'nationality' => $this->request['nationality'],
            'newsletter' => $this->request['newsletter']
        );

        $op = $this->dbHelper->insert('Player', $data);

        if ($op) return true;
        return false;
    }

} 